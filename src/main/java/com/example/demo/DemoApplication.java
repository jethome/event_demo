package com.example.demo;

import com.lark.oapi.Client;
import com.lark.oapi.service.corehr.v1.model.GetProcessFormVariableDataReq;
import com.lark.oapi.service.corehr.v2.model.BatchGetEmployeeReq;
import com.lark.oapi.service.corehr.v2.model.BatchGetEmployeeReqBody;
import com.lark.oapi.service.corehr.v2.model.BatchGetEmployeeResp;
import com.lark.oapi.service.hire.v1.model.ListTalentReq;
import com.lark.oapi.service.hire.v1.model.ListTalentResp;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) throws Exception {

        SpringApplication.run(DemoApplication.class, args);

//        Client client = Client.newBuilder("xx", "xx").build();
//
//        BatchGetEmployeeReqBody body = BatchGetEmployeeReqBody.newBuilder().employmentIds(new String[]{"7370898516401817114"}).fields(new String[]{"employment_id"}).
//                build();
//
//        BatchGetEmployeeReq req = BatchGetEmployeeReq.newBuilder()
//                .userIdType("people_core_id").batchGetEmployeeReqBody(body).build();
//
//        BatchGetEmployeeResp resp = client.corehr().v2().employee().batchGet(req);
//
//        System.out.println(resp.getData());
    }

}
