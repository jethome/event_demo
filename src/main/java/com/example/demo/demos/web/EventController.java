package com.example.demo.demos.web;

import com.lark.oapi.Client;
import com.lark.oapi.core.request.EventReq;
import com.lark.oapi.event.CustomEventHandler;
import com.lark.oapi.event.EventDispatcher;
import com.lark.oapi.sdk.servlet.ext.ServletAdapter;
import com.lark.oapi.service.corehr.CorehrService;
import com.lark.oapi.service.corehr.v1.model.P2EmploymentCreatedV1;
import com.lark.oapi.service.corehr.v1.model.P2EmploymentUpdatedV1;
import com.lark.oapi.service.corehr.v2.enums.BatchGetEmployeeUserIdTypeEnum;
import com.lark.oapi.service.corehr.v2.model.BatchGetEmployeeReq;
import com.lark.oapi.service.corehr.v2.model.BatchGetEmployeeReqBody;
import com.lark.oapi.service.corehr.v2.model.BatchGetEmployeeResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class EventController {

    @Autowired
    private ServletAdapter servletAdapter;

    @Autowired
    private Client client;

    private final EventDispatcher EVENT_DISPATCHER = EventDispatcher.newBuilder("xx", "xx").
            onP2EmploymentCreatedV1(new CorehrService.P2EmploymentCreatedV1Handler() {
                @Override
                public void handle(P2EmploymentCreatedV1 p2EmploymentCreatedV1) throws Exception {

                    System.out.println("create event");
                    String employmentId = p2EmploymentCreatedV1.getEvent().getEmploymentId();

                    //todo 业务代码
                    System.out.println(employmentId);



                }
            }).onP2EmploymentUpdatedV1(new CorehrService.P2EmploymentUpdatedV1Handler() {
                @Override
                public void handle(P2EmploymentUpdatedV1 p2EmploymentUpdatedV1) throws Exception {
                    System.out.println("update event");
                    String employmentId = p2EmploymentUpdatedV1.getEvent().getEmploymentId();

                    //todo 业务代码
                    System.out.println(employmentId);


                    //接口调用示例
                    BatchGetEmployeeResp resp = client.corehr().v2().employee().batchGet(BatchGetEmployeeReq.newBuilder().userIdType(BatchGetEmployeeUserIdTypeEnum.PEOPLE_COREHR_ID).
                            batchGetEmployeeReqBody(BatchGetEmployeeReqBody.newBuilder().fields(new String[]{"person_info.legal_name", "person_info.preferred_name"}).
                                    employmentIds(new String[]{employmentId}).build()).build());
                    if (!resp.success()) {
                        throw new RuntimeException(resp.getError().toString());
                    }

                    if (resp.getData().getItems().length != 0) {
                        System.out.println("legal_name: " + resp.getData().getItems()[0].getPersonInfo().getLegalName());
                        System.out.println("preferred_name: " + resp.getData().getItems()[0].getPersonInfo().getPreferredName());
                    }
                }
            }).onCustomizedEvent("对应eventType", new CustomEventHandler() {
                @Override
                public void handle(EventReq eventReq) throws Exception {
                    String eventStr = getEvent().getPlain();
                    //todo  json工具获取对应person_id或者附加信息id
                    String personId = "";

                    //todo对应处理

                }
            })

            .build();




    //3. 创建路由处理器
    @RequestMapping("/webhook/event")
    public void event(HttpServletRequest request, HttpServletResponse response) throws Throwable {
        //3.1 回调扩展包提供的事件回调处理器
        servletAdapter.handleEvent(request, response, EVENT_DISPATCHER);
    }

}
