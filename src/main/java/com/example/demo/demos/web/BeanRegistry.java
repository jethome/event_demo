package com.example.demo.demos.web;

import com.lark.oapi.Client;
import com.lark.oapi.sdk.servlet.ext.ServletAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class BeanRegistry {
    @Bean
    public Client GetLarkSDKClient() {
        return Client.newBuilder("cli_a5275ae68438900b", "tUHDmTLD8z1wr6s2jdtt5cjDnPt87Pbb").build();
    }

    // 注入扩展实例到 IOC 容器
    @Bean
    public ServletAdapter getServletAdapter() {
        return new ServletAdapter();
    }

}


